#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os

mnt = os.path.abspath('$basedir/mnt')

def mount(path):
    Command('mkdir {0}'.format(path))
    Test('$basedir/mockfs.py {0}'.format(path))


def umount(path):
    Test('fusermount -u {0}'.format(path))
    Command('rmdir {0}'.format(path))


def path(name):
    return os.path.join(mnt, name)
