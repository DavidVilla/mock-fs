#!/usr/bin/python
# -*- coding: utf-8 -*-

# This program is a fork from dictfs.py by Tobías Díaz Díaz-Chirón under the
# GPLv3 license.
# dictfs.py is avalaible at:
#   https://github.com/int-0/sillygames/blob/master/sillysamples/dictfs.py

# mockfs.py
#
# Copyright © 2011 Tobías Díaz Díaz-Chirón
# Copyright © 2011 David Villa Alises
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licen


import os
import stat
import errno
import time

import fuse
from fuse import Fuse
if not hasattr(fuse, '__version__'):
    raise RuntimeError("missing fuse.__version__, probably it's too old.")
fuse.fuse_python_api = (0, 2)

import logging
logging.basicConfig(filename='mockfs.log',
                    format='%(message)s',
                    level=logging.DEBUG)


# Only make one of this whe getstat() is called. Real FS has one per entry (file
# or directory).
class Stat(fuse.Stat):
    def __init__(self):
        self.st_mode = 0
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 0
        self.st_uid = 0
        self.st_gid = 0
        self.st_size = 0
        self.st_atime = 0
        self.st_mtime = 0
        self.st_ctime = 0


class Directory(dict):
    def ls(self):
        return self.keys()

    def has(self, entry):
        return entry in self

    def delete(self, entry):
        del self[entry]

    def stat(self):
        retval = Stat()
        retval.st_mode = stat.S_IFDIR | 0755
        retval.st_nlink = 2
        return retval


class File(object):
    def __init__(self, init=''):
        self.data = init

    def __len__(self):
        return len(self.data)

    def stat(self):
        retval = Stat()
        retval.st_mode = stat.S_IFREG | 0666  # rw- rw- rw-
        retval.st_nlink = 1
        retval.st_size = len(self)
        retval.st_ctime = int(time.time())
        retval.st_mtime = int(time.time())
        return retval


def log_method(fn):
    def wrapper(*args, **kargs):
        logging.debug("%s(%s)", fn.__name__,
                      str.join(', ', [repr(x) for x in args[1:]]))
        return fn(*args, **kargs)
    return wrapper


def ioerror(errno):
    retval = IOError()
    retval.errno = errno
    return retval


def oserror(errno):
    retval = OSError()
    retval.errno = errno
    return retval


class MockFS(Fuse):
    def __init__(self, *args, **kw):
        Fuse.__init__(self, *args, **kw)
        self.root = Directory()

    def split(self, path):
        return [x for x in path.split(os.sep) if x]

    def join(self, path):
        return os.sep + str.join(os.sep, path)

    def path2dir(self, path):
        retval = self.root
        for entry in self.split(path):
            if not entry in retval:
                return Directory()

            if not isinstance(retval[entry], dict):
                return Directory()

            retval = retval[entry]

        return retval

    def resolve(self, path, check=True):
        base, entry = os.path.split(path)
        parent = self.path2dir(base)
        if check and not parent.has(entry):
            logging.debug('resolve: not exists %s', path)
            raise ioerror(errno.ENOENT)

        return parent, entry

    @log_method
    def getattr(self, path):
        # Ask for root dir
        if path == '/':
            return self.root.stat()

        parent, entry = self.resolve(path)
        node = parent[entry]
        logging.debug('  getattr: found %s', node.__class__.__name__)
        return node.stat()

    ### FILESYSTEM FUNCTIONS ###
    @log_method
    def chmod(self, path, mode):
        return -errno.ENOSYS

    @log_method
    def chown(self, path, uid, gid):
        return -errno.ENOSYS

    @log_method
    def fsync(self, path, isFsyncFile):
        return -errno.ENOSYS

    @log_method
    def link(self, targetPath, linkPath):
        return -errno.ENOSYS

    @log_method
    def mkdir(self, path, mode):
        parent, entry = self.resolve(path, check=False)
        parent[entry] = Directory()

    @log_method
    def mknod(self, path, mode, dev):
        parent, entry = self.resolve(path, check=False)
        parent[entry] = File()

    # This method could maintain opened (or locked) file list and,
    # of course, it could check file permissions.
    # For now, only check if file exists...
    @log_method
    def open(self, path, flags):
        parent, entry = self.resolve(path)

        # No exception or no error means OK

    @log_method
    def readdir(self, path, offset):
        file_entries = ['.', '..']

        parent = self.path2dir(path)

        for entry in file_entries + parent.ls()[offset:]:
            yield fuse.Direntry(entry)

    @log_method
    def read(self, path, length, offset):
        parent, entry = self.resolve(path)
        node = parent[entry]

        # check ranges
        file_size = len(node)
        if offset >= file_size:
            return ''  # invalid range returns no data, instead error!

        # fix size
        if offset + length > file_size:
            length = file_size - offset

        return node.data[offset:offset + length]

    # In this example this method is the same as open(). This method
    # is called by close() syscall, it's means that if open() maintain
    # an opened-file list, or lock files, or something... this method
    # must do reverse operation (refresh opened-file list, unlock files...
    @log_method
    def release(self, path, flags):
        parent, entry = self.resolve(path)

    @log_method
    def rename(self, old_path, new_path):
        old_parent, old_entry = self.resolve(old_path)
        new_parent, new_entry = self.resolve(new_path, check=False)

        # make new link
        new_parent[new_entry] = old_parent[old_entry]

        # remove old
        self.unlink(old_path)

    @log_method
    def rmdir(self, path):
        parent, entry = self.resolve(path)
        parent.delete(entry)

    @log_method
    def truncate(self, path, size):
        parent, entry = self.resolve(path)
        node = parent[entry]

        if len(node) > size:
            # truncate file to specified size
            parent[entry] = File(node[:size])
        else:
            # add more bytes
            parent[entry].data += ' ' * (size - len(node))

    @log_method
    def unlink(self, path):
        parent, entry = self.resolve(path)
        parent.delete(entry)

    @log_method
    def write(self, path, buf, offset):
        parent, entry = self.resolve(path)
        f = parent[entry]

        # write data into file
        if offset > len(f):
            offset = offset % len(f)

        parent[entry].data = f.data[:offset] + str(buf)

        return len(buf)  # number of written bytes

    @log_method
    def utime(self, path, times, *args):
        parent, entry = self.resolve(path)
        try:
            parent[entry].ctime = times[0]
            parent[entry].mtime = times[1]
        except KeyError:
            return -errno.ENOENT

        return 0


def main():
    fs = MockFS(version = '%prog' + fuse.__version__, dash_s_do='setsingle')
    fs.parse(errex = 1)
    fs.main()

if __name__ == '__main__':
    main()
